import {Injectable} from 'angular2/core';
import {Section} from '../data/section'

const SECTIONS: Array<Section> = [
  new Section("Home", `
    <p>Sweet, sweet home.</p>
    <p>And some banners inside, right? or buttons, <strong>or jumbotron at least.</strong></p>
  `),
  new Section("Contact", `
    <p>Use mail, github or facebook to give us feedback.</p>
    <p>Here you should find some contact informations</p>
  `)
];

@Injectable()
export class SectionService {
  public getSections() {
    return Promise.resolve(SECTIONS);
  }
}
