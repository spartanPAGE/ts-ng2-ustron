export class Section {
  constructor(
    public title: String,
    public content: String
  ){}
}
