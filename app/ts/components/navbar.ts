import {Component, Input} from 'angular2/core';
import {Section} from '../data/section';

@Component({
  selector: 'navbar-component',
  templateUrl: 'app/ts/templates/navbar.html',
  styleUrls: [
    'app/ts/templates/styles/navbar.css'
  ],
})

export class NavbarComponent {
  @Input()
  sections: Array<Section>;
};
