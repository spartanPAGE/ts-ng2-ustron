import {Component, OnInit} from 'angular2/core';

import {NavbarComponent} from './navbar';
import {SectionsComponent} from './sections';

import {SectionService} from '../services/section';
import {Section} from '../data/section';


@Component({
  selector: 'app-component',
  templateUrl: 'app/ts/templates/app.html',
  styleUrls: [
    'app/ts/templates/styles/app.css'
  ],
  directives: [
    NavbarComponent,
    SectionsComponent
  ],
  providers: [
    SectionService
  ]
})

export class AppComponent implements OnInit {
  private sections: Array<Section>;

  constructor(
    private sectionService: SectionService
  ){}

  ngOnInit() {
    this.fetchSections();
  }

  private fetchSections() {
    this.sectionService.getSections().then(sections =>
      this.sections = sections
    );
  }
}
