import {Component, Input} from 'angular2/core';
import {Section} from '../data/section';

@Component({
  selector: 'sections-component',
  templateUrl: 'app/ts/templates/sections.html',
  styleUrls: [
    'app/ts/templates/styles/sections.css'
  ],
})

export class SectionsComponent {
  @Input()
  sections: Array<Section>;
};
